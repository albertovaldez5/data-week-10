# [[file:web.org::#running-a-template][app]]
from flask import Flask, render_template

app = Flask(__name__)

@app.route("/<name>")
def home(name: str):
    # template is in templates/index.html
    return render_template("index.html", name=name)

if __name__ == "__main__":
    # Use debug for hot-reloading when changes
    app.run(debug=True)
# app ends here
