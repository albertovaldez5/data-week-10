# [[file:web.org::#creating-a-script][scrapper]]
from splinter import Browser
from bs4 import BeautifulSoup as bs
from webdriver_manager.chrome import ChromeDriverManager

def init_browser():
    executable_path = ChromeDriverManager().install()
    return Browser("chrome", executable_path=executable_path, headless=False)

def scrape():
    browser = init_browser()
    url = "https://vancouver.craigslist.org/search/sss?query=guitar"
    browser.visit(url)
    html = browser.html
    soup = bs(html, 'html.parser')

    listing = {}
    try:
        listing['headline'] = soup.find("a", class_="result-title hdrlnk").text
        listing['price'] = soup.find("span", class_="result-price").text
        listing['hood'] = soup.find("span", class_="result-hood").text
    except:
        # temp
        pass
    return listing
# scrapper ends here
