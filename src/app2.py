# [[file:web.org::#creating-the-flask-app][app2]]
from flask import Flask, render_template
from flask_pymongo import PyMongo
from scraper import scrape

app = Flask(__name__)
uri = "mongodb://localhost:27017/craiglist"
mongo = PyMongo(app, uri=uri)

@app.route('/')
def home():
    data = scrape()
    mongo.db.results.update_one({}, {"$set":data}, upsert=True)
    # listing = mongo.db.listing.find_one()
    return render_template("index.html", listing=data)

if __name__ == "__main__":
    app.run(debug=True)
# app2 ends here
