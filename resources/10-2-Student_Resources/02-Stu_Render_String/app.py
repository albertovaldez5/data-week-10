# import necessary libraries
from flask import Flask, render_template
from pathlib import Path

# @TODO: Initialize your Flask app here
# CODE GOES HERE
app = Flask(__name__)

# @TODO:  Create a route and view function that takes in a string and renders index.html template
# CODE GOES HERE

templates = Path("templates")

@app.route("/")
def home():
    name = "Bob"
    return render_template("index.html", name=name)

@app.route("/bonus")
def bonus():
    name = "Bob"
    return render_template("bonus.html")

if __name__ == "__main__":
    app.run(debug=True)
