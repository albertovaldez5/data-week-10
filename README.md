- [HTML](#html)
- [Beautiful Soup](#beautiful-soup)
- [Find Elements](#find-elements)
- [Using Requests](#using-requests)
- [Flask Templates](#flask-templates)
  - [Running a Template](#running-a-template)
  - [Rendering Lists](#rendering-lists)
- [Scraping App](#scraping-app)
  - [Creating a Template](#creating-a-template)
  - [Creating a Script](#creating-a-script)
  - [Creating the Flask App](#creating-the-flask-app)
  - [Running the Scraping App](#running-the-scraping-app)
- [Mongosh Commands](#mongosh-commands)
  - [Mongo Shell commands](#mongo-shell-commands)
  - [Mongo in Python](#mongo-in-python)



<a id="html"></a>

# HTML

Hypertext Markup Language. The default format for documents displayed in a web browser.

HTML results in a tree-like structure, it contains branches that the browser reads and understands how to use. The result is called the DOM (Document Object Model).

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Page Title</title>
  </head>
  <body>
    <h1>This is a Heading</h1>
    <p>This is a paragraph.</p>
  </body>
</html>
```


<a id="beautiful-soup"></a>

# Beautiful Soup

Beautiful Soup is a Python library that converts the HTML into structured Python objects by parsing a string with the HTML contents.

```python
from bs4 import BeautifulSoup as bs
from pathlib import Path

resources = Path.cwd().parent / Path("resources")
with open(resources / "template.html") as file:
    html = file.read()

soup = bs(html, 'html.parser')
print(soup.prettify()[:100])
```

    <html>
     <head>
      <title>
       Top Ten Stories From 1996
      </title>
     </head>
     <body alink="#FFFFCE" bgc

We can print a list of all `p` elements in the soup.

```python
for i in soup.find_all('p')[:3]:
    print(i)
```

    <p><br/>
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td><img align="TOP" height="60" src="logos.gif" width="112"/></td>
    <td><img align="TOP" height="60" src="banner.gif" width="360"/></td>
    </tr>
    </table>
    </p>
    <p align="RIGHT"><b><tt>What were the biggest stories of the year?</tt></b><br/>
    <br/>
    <font size="2">It's a question journalists like to ask themselves at the end of every
                year. Now you can join in the process. Here are our selections for the top ten news
                stories of 1996.<br/>
    <br/>
                Disagree with our choices? Then tell us what stories you think were most compelling
                in the poll below.</font>
    </p>
    <p align="CENTER"><img align="MIDDLE" height="24" src="generic/topten.gif" vspace="5" width="263"/>
    </p>

If we use `find` we will get the first element of the list.

```python
print(soup.find('a'))
```

    <a href="topten/israel/israel.index.html" target="_top"><img align="MIDDLE" border="0" height="17" src="generic/1.gif" width="17"/></a>


<a id="find-elements"></a>

# Find Elements

We can explore the HTML document to find where in the page are the elements we are interested in. Then check with conditionals to extract the exact elements we want.

```python
headers = soup.find_all('td')
for h in headers:
    a = h.find('a')
    if a is not None:
        t = a.text
        if t is not None:
            print(t)
```

    
    Israel elects Netanyahu
    
    Crash of TWA Flight 800
    
    Russia elects Yeltsin
    
    U.S. elects Clinton
    
    Hutu-Tutsi conflict in central Africa
    
    Peace, elections in Bosnia
    
    U.S. base bombed in Saudi Arabia
    
    Centennial Olympic Games
    
    Advances against AIDS
    
    Unabomb suspect Ted Kaczynski arrested
    
    
    The top 10 stories according to our users
    Tell us what you think


<a id="using-requests"></a>

# Using Requests

We can make a request and get its text property to use with Beautiful Soup.

```python
import requests as rq
url = 'https://vancouver.craigslist.org/search/sss?query=guitar'
r = rq.get(url)
soup = bs(r.text, 'html.parser')
print(soup.title)
```

    <title>vancouver, BC for sale "guitar" - craigslist</title>

Then we can iterate over the items in the webpage, then use `try` and `except` to get all possible `a` elements with the `result-title` class. This means we are finding the container of each publication and then finding its title element and saving it to a list.

```python
results = soup.find_all('li', class_='result-row')
titles = []
for rs in results:
    try:
        title = rs.find('a', class_='result-title').text
        price = rs.a.span.text
        link = rs.a['href']
        titles.append([title, price, link])
    except AttributeError as e:
        pass

for i in titles[:5]:
    print(f"Title: {i[0]}, Price: {i[1]}, Link: {i[2]}")
```

    Title: BC RICH ACOUSTIC GUITAR, Price: $150, Link: https://vancouver.craigslist.org/van/msg/d/vancouver-bc-rich-acoustic-guitar/7524011460.html
    Title: ***  JOSE RAMIREZ  CLASSICAL GUITAR ***, Price: $1,350, Link: https://vancouver.craigslist.org/van/msg/d/chilliwack-jose-ramirez-classical/7524579014.html
    Title: 1970s Medallion Classical Acoustic Guitar w/case, Price: $300, Link: https://vancouver.craigslist.org/van/clt/d/vancouver-1970s-medallion-classical/7529652301.html
    Title: Jay Turner Basswood & Mahogany Acoustic Guitar, Price: $150, Link: https://vancouver.craigslist.org/van/msg/d/vancouver-jay-turner-basswood-mahogany/7529651983.html
    Title: Ibanez Gio Electric Guitar (6 String), Price: $200, Link: https://vancouver.craigslist.org/rds/atq/d/delta-central-ibanez-gio-electric/7533958497.html


<a id="flask-templates"></a>

# Flask Templates


<a id="running-a-template"></a>

## Running a Template

We are going to create a Flask app to run our scripts from the endpoints we expose in a server.

The templates **must** be in a folder named `templates` for `render_template` to load it by name like in the following example.

```python
from flask import Flask, render_template

app = Flask(__name__)

@app.route("/<name>")
def home(name: str):
    # template is in templates/index.html
    return render_template("index.html", name=name)

if __name__ == "__main__":
    # Use debug for hot-reloading when changes
    app.run(debug=True)

```

The route `home` will return the render of `index.html`.

```html
<body>
  <div class="container">
      <h1>Welcome, {{ name }}!</h1>
  </div>
</body>
```

Then we can start the app in `localhost` port `5000` by running the following command.

```shell
python app.py
```


<a id="rendering-lists"></a>

## Rendering Lists

We can use an expression in our template in order to render multiple values dynamically. In this case we are iterating over a list `{% for name in list %}` and a dictionary `{% for dogs in dogs %}`. We can access the dictionary values from the body of the loop `{{ dog.name }}, {{ dog.breed }}`.

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Animal Adoption!</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
  <div class="container text-center">
    <h1 class="jumbotron">Dogs up for Adoption!</h1>
    <div>
      <ul style="list-style: none;">

        <!-- Loop through the dictionary -->
        <div>
          <ul style="list-style: none;">
            {% for name in list %}
            <li> {{ name}} </li>
            {% endfor %}
          </ul>
        </div>
          <!-- Loop through the dictionary -->
        <div>
          <ul style="list-style: none;">
            {% for dogs in dogs %}
            <li> {{ dog.name }}, {{ dog.breed }}</li>
            {% endfor %}
          </ul>
        </div>

      </ul>
    </div>
  </div>
</body>
</html>
```

The Flask routes must reflect the template format. We add the arguments `list=member` and `dogs=dogs` to their respective `render_template` function calls.

```python

@app.route("/")
def home():
    """Render list"""
    members = ["Bob", "Alice", "Michael", "Gob"]
    return render_template("index2.html", list=members)

@app.route("/dict")
def dictionary():
    dogs = [
        {
            "name": "Cala",
            "breed": "Golden Retriever",
        },
        {
            "name": "Gorda",
            "breed": "Sheperd-mix",
        },
        {
            "name": "Vodka",
            "breed": "Yorkshire",
        },
    ]
    return render_template("index2.html", dogs=dogs)

```


<a id="scraping-app"></a>

# Scraping App


<a id="creating-a-template"></a>

## Creating a Template

We are creating an HTML template and naming it `index.html` and placing it in the `templates` directory.

```html
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Item for Sale</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>

<body>
  <div class="container-fluid">
    headline: {{listing.headline}}
    <br>
    price: {{listing.price}}
    <br>
    hood: {{listing.hood}}
    <br>
  </div>
  </div>
</body>
</html>
```


<a id="creating-a-script"></a>

## Creating a Script

We will create our scraping script to fit the needs of our data collection from a specific website. In this case we want all the listings in the Vancouver craiglist search for `guitar`. We&rsquo;ll get the headline, price and neighborhood of each posting and return a list.

```python
from splinter import Browser
from bs4 import BeautifulSoup as bs
from webdriver_manager.chrome import ChromeDriverManager

def init_browser():
    executable_path = ChromeDriverManager().install()
    return Browser("chrome", executable_path=executable_path, headless=False)

def scrape():
    browser = init_browser()
    url = "https://vancouver.craigslist.org/search/sss?query=guitar"
    browser.visit(url)
    html = browser.html
    soup = bs(html, 'html.parser')

    listing = {}
    try:
        listing['headline'] = soup.find("a", class_="result-title hdrlnk").text
        listing['price'] = soup.find("span", class_="result-price").text
        listing['hood'] = soup.find("span", class_="result-hood").text
    except:
        # temp
        pass
    return listing

```


<a id="creating-the-flask-app"></a>

## Creating the Flask App

We are naming our app `app2.py` and importing the scraping script named `scraper.py`. We are going to make a call of our scraping script each time we access the endpoint of `/` in our app, which means it&rsquo;s the `index.html` webpage. Then we&rsquo;ll call the scraping script to fetch exactly one posting and then display it in our template using `render_template`.

```python
from flask import Flask, render_template
from flask_pymongo import PyMongo
from scraper import scrape

app = Flask(__name__)
uri = "mongodb://localhost:27017/craiglist"
mongo = PyMongo(app, uri=uri)

@app.route('/')
def home():
    data = scrape()
    mongo.db.results.update_one({}, {"$set":data}, upsert=True)
    # listing = mongo.db.listing.find_one()
    return render_template("index.html", listing=data)

if __name__ == "__main__":
    app.run(debug=True)
```


<a id="running-the-scraping-app"></a>

## Running the Scraping App

We can start our server now.

```shell
python app2.py
```

If we go to the address specified `https://localhost:5000`, then the app will run and will scrape the data we want and display it on the webpage.


<a id="mongosh-commands"></a>

# Mongosh Commands

We are using `mongodb` to host our NoSQL database. Mongodb is a document based database that is fairly easy to use and it comes with a few tools we can use within the console to query our database.

We can run the following mongosh commands in order to take a look at our mongo database.

```shell
mongosh --eval "show dbs" | grep craiglist
```

    craiglist  56.00 KiB


<a id="mongo-shell-commands"></a>

## Mongo Shell commands

Run these inside the mongosh shell (mongosh).

```mongo
use craiglist
```

    test> switched to db craiglist
    craiglist> 

```mongo
show collections
```

    craiglist> results
    craiglist>

```mongo
db.results.find()
```

    craiglist> [
      {
        _id: ObjectId("63115f30dfc3639adc8e746c"),
        headline: 'Kids Nova guitar electric acoustic model 3301',
        hood: ' (VANCOUVER)',
        price: '$50'
      }
    ]
    craiglist>


<a id="mongo-in-python"></a>

## Mongo in Python

Here is some boilerplate code we can use to connect to a mongo database form Python. Note that the database and the Flask app are in different ports of our local server.

```python
from flask import Flask, render_template
from flask_pymongo import PyMongo
from scraper import scrape

app = Flask(__name__)
uri = "mongodb://localhost:27017/craiglist"
mongo = PyMongo(app, uri=uri)
print(mongo.db)
```

    Database(MongoClient(host=['localhost:27017'], document_class=dict, tz_aware=False, connect=False), 'craiglist')

We can access our database as a Python object whenever we want to interact with it. It&rsquo;s always a good idea to look at the documentation for the module <sup><a id="fnr.1" class="footref" href="#fn.1" role="doc-backlink">1</a></sup>.

```python
import pprint

print(mongo.db.list_collection_names())
pprint.pprint(mongo.db.results.find_one())
```

    ['results']
    {'_id': ObjectId('63115f30dfc3639adc8e746c'),
     'headline': 'Kids Nova guitar electric acoustic model 3301',
     'hood': ' (VANCOUVER)',
     'price': '$50'}

## Footnotes

<sup><a id="fn.1" class="footnum" href="#fnr.1">1</a></sup> <https://pymongo.readthedocs.io/en/stable/tutorial.html>
